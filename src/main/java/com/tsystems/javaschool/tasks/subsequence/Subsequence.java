package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        Iterator xIterator = x.iterator();
        Iterator yIterator = y.iterator();
        while (xIterator.hasNext()) {
            Object xNext = xIterator.next();
            while (yIterator.hasNext()) {
                Object s2 = yIterator.next();
                if (xNext.equals(s2)) {
                    xIterator.remove();
                    yIterator.remove();
                    break;
                }
                yIterator.remove();
            }
        }
        return x.isEmpty();
    }
}

