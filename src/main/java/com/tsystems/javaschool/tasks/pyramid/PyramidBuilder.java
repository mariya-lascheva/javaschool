package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        return puttingDigitsInMap(inputNumbers, numberLines(inputNumbers));
    }

    private int numberLines(List<Integer> inputNumbers) {

        for (Integer i : inputNumbers) {
            if (i == null) {
                throw new CannotBuildPyramidException();
            }
        }

        try {
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError E) {
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();

        int lines = 0;
        for (int j = 1; size >= j; j++) {

            size = size - j;
            ++lines;
            if (size <= j & size != 0) {
                throw new CannotBuildPyramidException();
            }
        }

        return lines;
    }

    private int[][] puttingDigitsInMap(List<Integer> inputNumbers, int lines) {
        int pyramideWidth = lines + (lines - 1);
        int[][] pyramidNull;
        pyramidNull = new int[pyramideWidth][lines];

        for (int i = 0; i < pyramideWidth; i++) {
            for (int j = 0; j < lines; j++) {
                pyramidNull[i][j] = 0;
            }
        }

        List<List<Integer>> pyramidDigit = new ArrayList<>();

        int y = 0;
        for (int x = 0; x <= lines - 1; x++) {
            List<Integer> row = new ArrayList<>();
            for (int i = 0; i <= x; i++) {
                row.add(inputNumbers.get(y));
                if (y < inputNumbers.size() - 1) {
                    y++;
                }
            }
            pyramidDigit.add(row);
        }

        for (int i = 0; i < pyramidDigit.size(); i++) {
            List<Integer> row = pyramidDigit.get(i);
            List<Integer> rowWithZero = new ArrayList<>();
            for (int j = 0; j < row.size(); j++) {
                rowWithZero.add(row.get(j));
                rowWithZero.add(0);
            }
            rowWithZero.remove(rowWithZero.size() - 1);
            pyramidDigit.set(i, rowWithZero);
        }

        int zeroNumber = 1;
        for (int i = pyramidDigit.size() - 2; i >= 0; i--) {
            List row = pyramidDigit.get(i);
            List rowWithZero = new ArrayList();
            for (int j = 0; j < row.size(); j++) {
                rowWithZero.add(row.get(j));
            }
            for (int z = 1; z <= zeroNumber; z++) {
                rowWithZero.add(0, 0);
            }
            for (int z = 1; z <= zeroNumber; z++) {
                rowWithZero.add(0);
            }
            zeroNumber++;

            pyramidDigit.set(i, rowWithZero);
        }

        int[][] pyramidDigitsArray = new int[pyramidDigit.size()][pyramidDigit.get(0).size()];

        for (int i = 0; i < pyramidDigit.size(); i++) {
            for (int j = 0; j < pyramidDigit.get(0).size(); j++) {
                int integer = pyramidDigit.get(i).get(j);
                pyramidDigitsArray[i][j] = integer;
            }
        }

        return pyramidDigitsArray;
    }

}
