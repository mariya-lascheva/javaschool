package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }

        try {
            List<String> simplify = stringToList(statement);
            String result = calcList(simplify).get(0);
            if(result.contains(".")) {
                return stripFpZeroes(String.format("%1$,.4f", Double.parseDouble(result))
                        .replace(",", "."));
            } else {
                return result;
            }
        } catch (IllegalArgumentException | ArithmeticException ex) {
            return null;
        }
    }

    private static String stripFpZeroes(String fpnumber) {
        int n = fpnumber.indexOf('.');
        if (n == -1) {
            return fpnumber;
        }
        if (n < 2) {
            n = 2;
        }
        String s = fpnumber;
        while (s.length() > n && s.endsWith("0")) {
            s = s.substring(0, s.length()-1);
        }
        return s;
    }

    private List<String> stringToList(String source) {
        String[] stringArray = source
                .replace("+", " + ")
                .replace("-", " - ")
                .replace("*", " * ")
                .replace("/", " / ")
                .replace("(", "( ")
                .replace(")", " )")
                .split(" ");
        List<String> list = new ArrayList<>();
        Collections.addAll(list, stringArray);
        return list;
    }

    private List<String> calcList(List<String> s) {
        if (s.size() == 1) {
            return s;
        } else if (s.size() == 3) {
            return calcTwoOperandsList(s);
        } else {
            if (s.contains("(")) {
                int leftP = s.indexOf("(");
                int rightP = s.indexOf(")");
                List<String> subList = s.subList(leftP + 1, rightP);
                String result = calcList(subList).get(0);
                int index = s.indexOf("(");

                for (int i = s.indexOf(")"); i > leftP; i--) {
                    s.remove(i);
                }
                s.remove(leftP);
                s.add(index, result);
            }
            if (s.contains("/")) {
                String result = calcTwoOperandsList(s.subList(s.indexOf("/") - 1, s.indexOf("/") + 2)).get(0);
                int index = s.indexOf("/") - 1;
                s.remove(index);
                s.remove(index);
                s.remove(index);
                s.add(index, result);
                calcList(s);
            }
            if (s.contains("*")) {
                List<String> subList = s.subList(s.indexOf("*") - 1, s.indexOf("*") + 2);
                String result = calcTwoOperandsList(subList).get(0);
                int index = s.indexOf("*") - 1;
                s.remove(index);
                s.remove(index);
                s.remove(index);
                s.add(index, result);
                calcList(s);
            }
            if (s.contains("+") || s.contains("-")) {
                String result = calcTwoOperandsList(s.subList(0, 3)).get(0);
                s.remove(0);
                s.remove(0);
                s.remove(0);
                s.add(0, result);
            }
            return calcList(s);
        }
    }

    private List<String> calcTwoOperandsList(List<String> s) {
        String left = s.get(0);
        String right = s.get(2);

        List<String> arrayList = new ArrayList<>();

        if (!left.contains(".") && !right.contains(".")) {
            int leftParsedInt = Integer.parseInt(left);
            int rightParsedInt = Integer.parseInt(right);

            int resultInt = 0;
            if ("+".equals(s.get(1))) {
                resultInt = leftParsedInt + rightParsedInt;
            }
            if ("-".equals(s.get(1))) {
                resultInt = leftParsedInt - rightParsedInt;
            }
            if ("*".equals(s.get(1))) {
                resultInt = leftParsedInt * rightParsedInt;
            }
            if ("/".equals(s.get(1))) {
                if (leftParsedInt % rightParsedInt != 0) {
                    arrayList.add(String.valueOf(leftParsedInt / (double) rightParsedInt));
                } else {
                    resultInt = leftParsedInt / rightParsedInt;
                }
            }
            arrayList.add(String.valueOf(resultInt));
            return arrayList;
        } else {

            double leftParsed;
            double rightParsed;
            leftParsed = Double.parseDouble(left);
            rightParsed = Double.parseDouble(right);

            double result = 0.0;
            if ("+".equals(s.get(1))) {
                result = leftParsed + rightParsed;
            }
            if ("-".equals(s.get(1))) {
                result = leftParsed - rightParsed;
            }
            if ("*".equals(s.get(1))) {
                result = leftParsed * rightParsed;
            }
            if ("/".equals(s.get(1))) {
                result = leftParsed / rightParsed;
            }

            arrayList.add(String.valueOf(result));
            return arrayList;
        }
    }

}
